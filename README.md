# Wordpress with sqlite enabled
Thanks to the [wp-sqlite-db](https://github.com/aaemnnosttv/wp-sqlite-db), I was able to make this quickly for the quick deployment of the version.
By the way, I added [WPGatsby](https://wordpress.org/plugins/wp-gatsby/) and [WPGraphQL](https://github.com/wp-graphql/wp-graphql/releases) too for Gatsby integration.
